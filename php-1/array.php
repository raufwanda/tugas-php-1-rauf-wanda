<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php
    echo "<h3> Soal 1 </h3>";
    /* 
            SOAL NO 1
            Kelompokkan nama-nama di bawah ini ke dalam Array.
            Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
            Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
        */
    $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
    print_r($kids);

    echo "<h3> </h3>";

    $Adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
    print_r($Adults);


    echo "<h3> Soal 2</h3>";
    /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        
            */

    echo "Total kids " . count ($kids);
    echo "<ol>";
    echo "<li>" . $kids[0] ."</li>";
    echo "<li>" . $kids[1] ."</li>";
    echo "<li>" . $kids[2] ."</li>";
    echo "<li>" . $kids[3] ."</li>";
    echo "<li>" . $kids[4] ."</li>";
    echo "<li>" . $kids[5] ."</li>";
    echo "</ol>";
    

    echo "Total Adults " . count ($Adults);
    echo "<ol>";
    echo "<li>" . $Adults[0] ."</li>";
    echo "<li>" . $Adults[1] ."</li>";
    echo "<li>" . $Adults[2] ."</li>";
    echo "<li>" . $Adults[3] ."</li>";
    echo "<li>" . $Adults[4] ."</li>";
    echo "</ol>";

    echo "<h3> Soal 3</h3>";
    /*
            SOAL No 3
            Susun data-data berikut ke dalam bentuk Asosiatif Array didalam Array Multidimensi
            Name: "Will Byers"
            Age: 12,
            Aliases: "Will the Wise"
            Status: "Alive"

            Name: "Mike Wheeler"
            Age: 12,
            Aliases: "Dungeon Master"
            Status: "Alive"

            Name: "Jim Hopper"
            Age: 43,
            Aliases: "Chief Hopper"
            Status: "Deceased"

            Name: "Eleven"
            Age: 12,
            Aliases: "El"
            Status: "Alive"

       
        */
    
    $biodata = [
        ["name" => "Will Byers", "Age" => 12, "Aliases" => "Will the Wise", "Status" => "Alive"],
        ["name" => "Mike Wheeler", "Age" => 12, "Aliases" => "Dungeon Master", "Status" => "Alive"],
        ["name" => "Jim Hoppers", "Age" => 43, "Aliases" => "Chief Hopper", "Status" => "Deceased"],
        ["name" => "Eleven", "Age" => 12, "Aliases" => "EI", "Status" => "Alive"]
    ];

    echo "<pre>";
    print_r($biodata);
    echo "<pre>";    
    ?>
</body>

</html>